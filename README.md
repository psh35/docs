# Raven public documentation

This repository contains the source for the Raven public documentation.

## Developing locally

The guidebook uses mkdocs to convert Markdown source into a static website. You can run it from either the dockerised setup or your own local environment:

### Using Docker

```console
$ docker compose up
```
The local documentation is now available at http://localhost:8000/.

### Using your local environment

Install mkdocs using pip:

```bash
pip3 install --user -r requirements.txt
```

You can then serve a site locally for development via ``mkdocs``:

```bash
mkdocs serve
```

The local documentation is now available at http://localhost:8000/.

## Hosting

The guidebook is hosted as [a
project](https://readthedocs.org/projects/ucam-raven/) on readthedocs.org (RTD)
and is automatically built on each commit to master. RTD natively supports
mkdocs documentation and so, beyond informing RTD that this is a mkdocs project,
no configuration is required to build the docs.

Currently, the RTD configuration is manual. To gain access to the project, create
a RTD account and ask an existing administrator to add you as a maintainer to
the project.

RTD knows when to build the documentation by means of a
[webhook](https://docs.readthedocs.io/en/stable/webhooks.html). This has to be
manually added to the Developer Hub project as an integration.  The Developer
Hub project should be configured to POST to the RTD webhook on push events.
[GitLab-specific
documentation](https://docs.readthedocs.io/en/stable/webhooks.html#gitlab) is
provided by RTD.

There are [a set of custom
domains](https://readthedocs.org/dashboard/ucam-raven/domains/) configured for
the documentation in the RTD project.

The Raven deployment provisions a CNAME record pointing to `readthedocs.io`
inside the `gcloud.automation.uis.cam.ac.uk` domain. This is added to the RTD
panel as a custom domain but not as the "primary" domain.

Instead we perform the usual workaround in the ipreg database to register
`docs.raven.cam.ac.uk`. Specificially:

1. Create a vbox record (via
   [vbox_ops](https://jackdaw.cam.ac.uk/ipreg/vbox_ops)) for the CNAME record
   in `gcloud.automation.uis.cam.ac.uk`.
2. Add a CNAME (via [cname_ops](https://jackdaw.cam.ac.uk/ipreg/cname_ops))
   pointing from `docs.raven.cam.ac.uk` to the `gcloud.automation.uis.cam.ac.uk`
   vbox.

Empirically, this is sufficient proof to RTD that we control the domain and RTD
then issues a TLS certificate for us.
